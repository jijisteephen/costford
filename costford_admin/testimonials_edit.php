<?php
session_start();
ob_start();
require_once("../pji-load.php");
defined('PJT_EXE') or die('Access Restricted , Website is down for maintenance.');
require_once(PJI_STP_DIR . PJI_COR_DIR . "utility.php");
require_once(PJI_STP_DIR . PJI_COR_DIR . "admin-utility.php");
$table_main = $db_sfx . "testimonials";
$table_sfx = "testimonials_";
$dyn_folder = PJI_STP_DIR . PJI_IMG_DIR . PJI_HSR_DIR;
check_login();
$tabm = 7;
$tab = 0;
$tabl = 14;
if(isset($_REQUEST['id']));
{
    $id = $_REQUEST['id'];
}

?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <?php include("includes/header1.php");?>
    <title>Anchal | Testimonials Edit</title>
</head>
<script>
    function vald()
    {
        var desc = CKEDITOR.instances.desc.getData();
        if(desc == "")
        {
            alert("Please Enter Description");
            return false;
        }
    }
</script>

<!-- END HEAD -->
<?php
if (isset($_POST['add']) == 'add') 
{  

        
        $db_data[$table_sfx.'name'] = $_POST['name'];
         $db_data[$table_sfx.'place'] = $_POST['place'];
        $db_data[$table_sfx.'content'] =htmlentities($_POST['desc'],ENT_QUOTES);
        $db_data['status'] = $_POST['status'];
        $update_data = update_data($table_main,$db_data,"testimonials_id=$id",true,$conn); 
        if ($update_data == 1 ) 
        {
        ?>
        <script type="text/javascript">
        alert('Successfully added'); //sucess , error, info
        setTimeout("window.location = 'testimonials_list.php'", 100);
        </script>
        <?php
    } 
    else
    {
    ?>
        <script type="text/javascript">
        alert( 'Error occured, Try Again.');
        </script>
    <?php
    }
}
?>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN HEADER -->
        <?php include("includes/header.php");?>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
        <!-- BEGIN SIDEBAR -->
           
<?php include("includes/sidebar.php");?>
        <!-- BEGIN PAGE -->  
        <div class="page-content">
            <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <div id="portlet-config" class="modal hide">
                <div class="modal-header">
                    <button data-dismiss="modal" class="close" type="button"></button>
                    <h3>portlet Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here will be a configuration form</p>
                </div>
            </div>
            <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
         <br/>
         <br/>
            
                <div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet box blue tabbable">
                            <div class="portlet-title">
                                <div class="caption">
                                    <span class="hidden-480">Edit testimonials</span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                        
                                <!-- BEGIN FORM-->
                         <br/><br/>
                        <?php
                            $rows = select_a_row($table_main,"testimonials_id=$id",$conn,true);
                            foreach($rows as $row)
                            {
                                        $id = $row['testimonials_id'];
                                        $place = $row[$table_sfx.'place'];
                                        $name = $row[$table_sfx.'name'];
                                        $content = $row[$table_sfx.'content'];
                                        $status = $row['status'];
                            }
                            ?>        
    <form action="" id="form_sample_1" class="form-horizontal" method="post" enctype="multipart/form-data">

                                      <div class="control-group">
                                        <label class="control-label">Name<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="name" class="span10 m-wrap" required="required" value="<?php echo $name;?>"/>
                                            
                                        </div>
                                       </div>
                                        <div class="control-group">
                                        <label class="control-label">Place<span class="required">*</span></label>
                                        <div class="controls">
                                            <input type="text" name="place" class="span10 m-wrap" required="required" value="<?php echo $place;?>"/>
                                            
                                        </div>
                                       </div>
                                   
                                         <div class="row-fluid">
                                                    <div class="span12 ">
                                                        <div class="control-group">
                                                            <label class="control-label" >Testimonials<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <textarea class="span12 ckeditor m-wrap" id="desc" name= "desc" rows="6" required="required" ><?php echo $content;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            
                            
                                  
                                    <div class="control-group">
                                        <label class="control-label">Status<span class="required">*</span></label>
                                        <div class="controls">
                                            <select id="status" name="status"  class="span5 m-wrap" required>
                                           
                                           <?php if($status == 1)
                                           {
                                                ?>
                                                  <option value="1">Published</option>
                                                  <option value="0">Unpublished</option>
                                                <?php
                                            } ?>
                                             <?php if($status == 0)
                                           {
                                                ?>
                                                  <option value="0">Unpublished</option>
                                                  <option value="1">Published</option>
                                                    
                                                <?php
                                            } ?>
                                           
                                            </select>
                                        </div>
                                    </div>
                               
                                    <div class="form-actions">
                                        <button type="submit" id="add" name="add" value="add" class="btn blue" onclick="return vald()"><i class="icon-ok"></i>Save</button>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
       
            </div>
            <!-- END PAGE CONTAINER-->
        </div>
        <!-- END PAGE -->  
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
<?php include("includes/footer.php");?>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/select2/select2.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <script type="text/javascript" src="../assets/plugins/ckeditor/ckeditor.js"></script> 
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/scripts/app.js"></script>
    <script src="../assets/scripts/form-components.js"></script>  

    <script src="../assets/scripts/form-validation.js"></script>    
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        jQuery(document).ready(function() {       
           // initiate layout and plugins
           App.init();
           FormComponents.init();
        });
    </script>
    <!-- END JAVASCRIPTS -->   
</body>
<!-- END BODY -->
</html>