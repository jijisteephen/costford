<?php
//error_reporting(0);
/*******************************/	
// File Upload function starts //
/******************************/ 
{

	//Start Single image upload function	 
	//Image upload function (file array, foldername/, maximum size, valid extensions array)
	function image_upload($img,$folder,$suffix,$max_size,$valid_ext)
	{
		//Set the values for upload
		//Allowed file size
		
		
		if((empty($max_size))||($max_size<=0))
		{
			//byte * kb * mb = 1mb 
			$max_size=1024*1024*2;
		}		 		
		//This variable contains an array of mime types.  It is used by the Upload to help identify allowed file types.
	//	$valid_mime = array("image/jpeg","image/jpg","image/pjpg","image/x-png","image/png","image/gif","image/bmp", "image/x-windows-bmp");		
		if(empty($valid_ext))
		{
		//Allowed extension
		$valid_ext=array("jpg","jpeg","gif","png","bmp");
		}
		
		if(empty($suffix))
		{
			$suffix="";	
		}
		
		//Get the file values
	    $file_name=$img['name'];		
		$file_size=$img['size']; //file size in bytes	
		$file_temp=$img['tmp_name'];
		$file_error=$img["error"];	
		$file_ext=getext($file_name); //get the last extension of file
	//	$file_mime = $img['type']; //Get mime type of file
	
	if((in_array($file_ext,$valid_ext))&&($file_size<=$max_size))
		{
			//Check file error
		    if ($file_error > 0)
			{
				if(isset($img['tmp_name']))
				{
					unlink($img['tmp_name']);
				}				
				echo('Image upload is error.');
				return false;
			 }
			
			//Create new name
			$crt_time=microtime();
			$crt_time=$suffix.$crt_time;
			$crt_time=clearstring($crt_time);	 //Make a string lowercase + Remove space, Chars	
			$new_filename= $crt_time.".".$file_ext;
			
			//check file in existing folder
		    if(file_exists($folder.$new_filename))
			{
			
				//rename new file name			
				$crt_date=time();
				$crt_date=clearstring($crt_date);					
				$crt_date_time=$suffix.$crt_date.$crt_time;
				$crt_date_time=clearstring($crt_date_time);
				$new_filename=$crt_date_time.".".$file_ext;				
			}
			
			if(move_uploaded_file($file_temp,$folder.$new_filename))
			{
			
				return $new_filename;
			}
		}	
		else
		{
			 return false;
		}
	}	
	//End Single image upload function
	
	// Start Multiple image upload function
	// Multiple Image upload function
	//its return image name array .If files is error, corresponding array value is empty
	// multiple_image_upload(file array, foldername/, maximum size, valid extensions array)
	function multiple_image_upload($img,$folder,$suffix,$max_size,$valid_ext)
	{
			//Set the values for upload
			//Allowed file size
			if((empty($max_size))||($max_size<=0))
			{
				//byte * kb * mb = 1mb 
				$max_size=1024*1024*2;
			}		 		
			//This variable contains an array of mime types.  It is used by the Upload to help identify allowed file types.
			//$valid_mime = array("image/jpeg","image/jpg","image/pjpg","image/x-png","image/png","image/gif","image/bmp", "image/x-windows-bmp");		
			if(empty($valid_ext))
			{
			//Allowed extension
			$valid_ext=array("jpg","jpeg","gif","png","bmp");
			}
			
			if(empty($suffix))
			{
				$suffix="";	
			}
			$new_filenames=array();
			$file_nos=count($img['name']);
			for($i=0;$i<$file_nos;$i++)
			{
				//Get the file values
				$file_name=$img['name'][$i];		
				$file_size=$img['size'][$i]; //file size in bytes	
				$file_temp=$img['tmp_name'][$i];	
				$file_error=$img["error"][$i];
				$file_ext=getext($file_name); //get the last extension of file
				//	$file_mime = $img['type']; //Get mime type of file
				
				if((in_array($file_ext,$valid_ext))&&($file_size<=$max_size))
					{
						//Check file error
						if ($file_error > 0)
						{
							unlink($img['tmp_name']);
							echo('Image upload is error.');
							return false;
						 }
						
						//Create new name
						$crt_time=microtime();
						$crt_time=$suffix.$crt_time;
						$crt_time=clearstring($crt_time);	 //Make a string lowercase + Remove space, Chars	
						$new_filename= $crt_time.".".$file_ext;
						
						//check file in existing folder
						if(file_exists($folder.$new_filename))
						{
							//rename new file name			
							$crt_date=time();
							$crt_date=clearstring($crt_date);					
							$crt_date_time=$suffix.$crt_date.$crt_time;
							$crt_date_time=clearstring($crt_date_time);
							$new_filename=$crt_date_time.".".$file_ext;				
						}
						
						if(move_uploaded_file($file_temp,$folder.$new_filename))
						{
							$new_filenames[]=$new_filename;
						}
						
					}	
					else
					{
						 $new_filenames[]=""; //NULL
					}
				} //for ends		
				return $new_filenames;
	}
	//End Multiple image upload function
	
	/*
         * Single file upload function (file array, foldername/, maximum size, valid extensions array)
         */
	function file_upload($img,$folder,$suffix,$max_size,$valid_ext)
	{
		//Set the values for upload
		//Allowed file size
		
		if((empty($max_size))||($max_size<=0))
		{
			//byte * kb * mb = 1mb 
			$max_size=1024*1024*25;
		}		 		
		//This variable contains an array of mime types.  It is used by the Upload to help identify allowed file types.
	//	$valid_mime = array("image/jpeg","image/jpg","image/pjpg","image/x-png","image/png","image/gif","image/bmp", "image/x-windows-bmp");		
		if(empty($valid_ext))
		{
		//Allowed extension
		$valid_ext=array("wav","mp3","wmv","mp4");
		}
		
		if(empty($suffix))
		{
			$suffix="";	
		}
		
		//Get the file values
		$file_name=$img['name'];		
		$file_size=$img['size']; //file size in bytes	
		$file_temp=$img['tmp_name'];
		$file_error=$img["error"];	
		$file_ext=getext($file_name); //get the last extension of file
	//	$file_mime = $img['type']; //Get mime type of file
	
	if((in_array($file_ext,$valid_ext))&&($file_size<=$max_size))
		{
			//Check file error
		    if ($file_error > 0)
			{
				if(isset($img['tmp_name']))
				{
					unlink($img['tmp_name']);
				}				
				echo('File upload is error.');
				return false;
			 }
			
			//Create new name
			$crt_time=microtime();
			$crt_time=$suffix.$crt_time;
			$crt_time=clearstring($crt_time);	 //Make a string lowercase + Remove space, Chars	
			$new_filename= $crt_time.".".$file_ext;
			
			//check file in existing folder
		    if(file_exists($folder.$new_filename))
			{
			
				//rename new file name			
				$crt_date=time();
				$crt_date=clearstring($crt_date);					
				$crt_date_time=$suffix.$crt_date.$crt_time;
				$crt_date_time=clearstring($crt_date_time);
				$new_filename=$crt_date_time.".".$file_ext;				
			}
			
			if(move_uploaded_file($file_temp,$folder.$new_filename))
			{
			
				return $new_filename;
			}
		}	
		else
		{
			 return false;
		}
	}
	//End Single file upload function
function file_uploadstudent($img,$folder,$suffix,$max_size,$valid_ext)
	{
		//Set the values for upload
		//Allowed file size
		
		if((empty($max_size))||($max_size<=0))
		{
			//byte * kb * mb = 1mb 
			$max_size=1024*1024*2;
		}		 		
		//This variable contains an array of mime types.  It is used by the Upload to help identify allowed file types.
	//	$valid_mime = array("image/jpeg","image/jpg","image/pjpg","image/x-png","image/png","image/gif","image/bmp", "image/x-windows-bmp");		
		if(empty($valid_ext))
		{
		//Allowed extension
		$valid_ext=array("rtf","doc","docx","txt","pdf");//"jpg","jpeg","gif","png","bmp"
		}
		
		if(empty($suffix))
		{
			$suffix="";	
		}
		
		//Get the file values
		$file_name=$img['name'];		
		$file_size=$img['size']; //file size in bytes	
		$file_temp=$img['tmp_name'];
		$file_error=$img["error"];	
		$file_ext=getext($file_name); //get the last extension of file
	//	$file_mime = $img['type']; //Get mime type of file
	
	if((in_array($file_ext,$valid_ext))&&($file_size<=$max_size))
		{
			//Check file error
		    if ($file_error > 0)
			{
				if(isset($img['tmp_name']))
				{
					unlink($img['tmp_name']);
				}				
				echo('Image upload is error.');
				return false;
			 }
			
			//Create new name
			$crt_time=microtime();
			$crt_time=$suffix.$crt_time;
			$crt_time=clearstring($crt_time);	 //Make a string lowercase + Remove space, Chars	
			$new_filename= $crt_time.".".$file_ext;
			
			//check file in existing folder
		    if(file_exists($folder.$new_filename))
			{
			
				//rename new file name			
				$crt_date=time();
				$crt_date=clearstring($crt_date);					
				$crt_date_time=$suffix.$crt_date.$crt_time;
				$crt_date_time=clearstring($crt_date_time);
				$new_filename=$crt_date_time.".".$file_ext;				
			}
			
			if(move_uploaded_file($file_temp,$folder.$new_filename))
			{
			
				return $new_filename;
			}
		}	
		else
		{
			 return false;
		}
	}
	//End Single file upload function

/* get the last extension off a file name */
	function getext($file_name)
	{
		$chunks = explode('.', $file_name);
		$chunksCount = count($chunks) - 1;
		if($chunksCount > 0)
		{
			return strtolower(trim($chunks[$chunksCount]));
		}
		return false;
	}
//clear symbols, CAPS in a string
	function clearstring($string)
	{
		//Create a new name
		$chars = array(" ","~", "!", "@", "#", "$", "%", "^", "&", "*", "(",")","-","+","=","{","}","[","]","|","\\",":",";","'","\"","<",">",",","/","?",".","_");// remining ""	
		$string=strtolower(str_replace($chars,"",$string));
		if(!empty($string))
		{
			return $string;
		}
		return false;
	}
}
/*******************************/	
// File Upload function Ends   //
/******************************/

/**********************************/	
// DATE And TIME function Starts  //
/*********************************/
{
	//Set time zone()
	function set_time_zone($tz)
		{
			date_default_timezone_set('Asia/Kolkata');
		}
	//Get User Date and time
		function get_time()
		{
			set_time_zone("IN");
			return date('Y-m-d H:i:s'); //2013-06-21 15:14:36
		}
		function get_timestamp()
		{
			set_time_zone("IN");
			return date('Y-m-d H:i:s'); //2013-06-21 15:14:36
		}
		
		function get_db_date()
		{
			set_time_zone("IN");
			return date('Y-m-d'); //2013-06-21
		}
		
		//Get Time @ 12HR
		function get_time_12()
		{
			set_time_zone("IN");
			return date('h:i A'); //5:14 PM
		}
		
		//Get Time @ 24HR
		function get_time_24()
		{
			set_time_zone("IN");
			return date('h:i:s'); //15:14:25
		}
		
		//Get Date
		function get_date()
		{
			set_time_zone("IN");
			return date('d-m-Y'); //21-06-2013
		}
		
	/** Internal method to get the current time.
 	* @return The current time in seconds with microseconds (in float format).  */
    function getMicroTime()
    {
      list($msec, $sec) = explode(' ', microtime());	  
      return floor($sec / 1000) + $msec;
    }	
	
	//Convert to DB Formated Date - START
	//dd-mm-yyyy - > yyyy-mm-dd  &  yyyy-mm-dd -> dd-mm-yyyy
	function swap_db_date($val_date="",$sep="-") 
	{
		if($val_date=="")
		{
			$val_date=get_date();
			$val_date=explode($sep,$val_date);
		}
		else
		{
			$val_date=explode($sep,$val_date);
		}
		$db_date=$val_date[2]."-".$val_date[1]."-".$val_date[0];
		return $db_date;
	}
}
/**********************************/	
// DATE And TIME function ENDS	  //
/*********************************/

/**********************************/	
// USER DATA function STARTS	  //
/*********************************/
{
//Get User IP
	function get_ip()
	{
		//Check for real ip
		if (isset($_SERVER["HTTP_CLIENT_IP"]))
		{
			return $_SERVER["HTTP_CLIENT_IP"];
		}
		else if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
		{
			return $_SERVER["HTTP_X_FORWARDED_FOR"];
		}
		else if (isset($_SERVER["HTTP_X_FORWARDED"]))
		{
			return $_SERVER["HTTP_X_FORWARDED"];
		}
		else if (isset($_SERVER["HTTP_FORWARDED_FOR"]))
		{
			return $_SERVER["HTTP_FORWARDED_FOR"];
		}
		else if (isset($_SERVER["HTTP_FORWARDED"]))
		{
			return $_SERVER["HTTP_FORWARDED"];
		}
		else
		{
			return $_SERVER["REMOTE_ADDR"];
		}
	}

//Get User Browser Details
	function get_br()
	{
		return $_SERVER['HTTP_USER_AGENT'];
	}
//Get User Mac
	function get_mac()
	{
			$browser = get_browser(null, true);
			$ipAddress=get_ip();
			$macAddr=false;
			#run the external command, break output into lines
			$arp=`arp -a $ipAddress`;
			$lines=explode("\n", $arp);
			#look for the output line describing our IP address
			foreach($lines as $line)
			{
			  // echo($line);
			   $cols=preg_split('/\s+/', trim($line));
			   if ($cols[0]==$ipAddress)
			   {
				 $macAddr=$cols[1];
			   }  
			}
			$macAddr=strtoupper($macAddr);
			return $macAddr;
	}
}
/**********************************/	
// USER DATA function  ENDS	 	 //
/*********************************/

/**************************/	
//  DB functions Starts   //
/*************************/	
{
//Insert single row to db function START

	function insert_data($table,$data,$insert=true,$conn)
		{  
		  
			$fields=array_keys($data);
			
			$fields=implode(', ', $fields);
			$value=array_values($data);
			foreach($value as $ival)
			{
				$values[]=mysqli_real_escape_string($conn,$ival);
			}
			$values=implode('\', \'', $values);	
			$values="'".$values."'";
		    $sql_insert_data="INSERT INTO ".$table." (".$fields.") VALUES (".$values.")";

			if($insert==true)
			{
				return exe_query($sql_insert_data,$conn);
			}
			else
			{
				return $sql_insert_data;
			}
		}
//Insert single row to db function ENDS

//Insert multiple rows to db in single DB connection function START	

	function insert_multiple_data($table,$datas,$insert=true,$conn)
		{
			$run_flag=1;
			$insert_value=array();
			unset($insert_value);
			$sql_insert_multiple_data="";
			foreach($datas as $field_data)
				{					
					if($run_flag==1)
					{
						$fields=array_keys($field_data);
						$fields=implode(', ', $fields);
						$run_flag=0;
					}				
					$value=array_values($field_data);
					$values=array();
					foreach($value as $ival)
					{
						$values[]=mysqli_real_escape_string($conn,$ival);
					}
					$values=implode('\', \'', $values);	
					$insert_value[]="('".$values."')";	
				}
			$insert_value=implode(', ', $insert_value);
			$sql_insert_multiple_data="INSERT INTO ".$table." (".$fields.") VALUES ".$insert_value;
			if($insert==true)
			{
				return exe_query($sql_insert_multiple_data,$conn);
			}
			else
			{
				return $sql_insert_multiple_data;
			}
			
			
		}
//Insert multiple rows to db at single DB connection function ENDS	
	
//Update single row in db function START

	function update_data($table,$data,$where_cause,$update=true,$conn)
		{	
			$run_flag=1;
			$update_value=array();
			unset($update_value);
			$update_fields="";
			foreach($data as $field=>$field_data)
				{
					if($run_flag==1)
					{
						$update_fields=" ".$field."='".$field_data."' ";
						$run_flag=0;
					}	
					else if($run_flag==0)
					{
						$update_fields.=" , ".$field."='".$field_data."' ";		
					}	
				}
	  			 $sql_update_data="UPDATE ".$table." SET ".$update_fields." WHERE ".$where_cause." ";

	      
		   if($update==true)
			{
				return exe_query($sql_update_data,$conn);
			}
			else
			{
				return $sql_update_data;
			}
			
		}
//Update single row in db function ENDS

//Select rows in db function START
	function select_a_row($table,$where_cause,$conn,$run=true,$sql=false)
		{
		if($sql==true)
		   {
			   $sel_cnt_sql=$where_cause;
		   }
		 else if($where_cause=="")
			{
				$sel_cnt_sql="SELECT * FROM ".$table."";	
			}
		else if($where_cause!="")
			{
	    $sel_cnt_sql="SELECT * FROM ".$table." WHERE ".$where_cause."  LIMIT 0,1";

			}
		//Run sql condetion	
		if($run==true)
			{
				$sel_cnt_result=exe_query($sel_cnt_sql,$conn);
				return row_query($sel_cnt_result);
			}
		else
			{
				return $sel_cnt_sql;
			}
			
		}

//Select a row in db function ENDS


//Select all rows in db function START

	function select_all_rows($table,$where_cause,$conn,$run=true,$sql=false)
		{ 
		 if($sql==true)
		   {
			   $sel_cnt_sql=$where_cause;
		   }
		else if($where_cause=="")
			{
				$sel_cnt_sql="SELECT * FROM ".$table."";
			}
		else if($where_cause!="")
			{
		 $sel_cnt_sql="SELECT * FROM ".$table." WHERE ".$where_cause;

			}
		//Run sql condetion	
		if($run==true)
			{
				$sel_result=exe_query($sel_cnt_sql,$conn);
				return row_query($sel_result);
			}
		else
			{
				return $sel_cnt_sql;
			}			
		}
//Select all rows in db function ENDS



//Delete row in db function START
	
	function delete_data($table,$where_cause="",$conn)
		{	
			//$del_row=delete_select($table,$where_cause);
			$sql_delete_data="DELETE FROM ".$table." WHERE ".$where_cause."";
			$del_result= exe_query($sql_delete_data,$conn);
			$del_row=mysqli_affected_rows($conn);
			$delete_result['result']=$del_result;
			$delete_result['rows']=$del_row;
			return($delete_result);
		}
//Delete row in db function ENDS//Delete row in db function ENDS	

//Select row in db for delete function START -Now not in USE
	function delete_select($table,$where_cause="",$conn)
		{				
			$sql_sel_data="SELECT * FROM ".$table." WHERE ".$where_cause." ";
			$sql_sel_result=exe_query($sql_sel_data,$conn);
			$sel_rows=row_query($sql_sel_result);			
			return $sel_rows;
		}
//Select row in db for delete function ENDS

//Clear Sql data for pevent sql injection FUNCTION START	
	function sql_clear_data($sql_field_data)
		{
			$slash_data=addslashes($sql_field_data);
			return($slash_data);
		}
//Clear Sql data for pevent sql injection FUNCTION ENDS

//RUN SQL COMMENT IN PHP  FUNCTION START
	function exe_query($sql,$conn)
		{
			return mysqli_query($conn,$sql);
		}
//RUN SQL COMMENT IN PHP  FUNCTION ENDS

//SQL ROWS CONVERT TO ARRAY START
	function row_query($sql_result)
		{
			$selected_rows=array();
			unset($selected_rows);
			while($row=mysqli_fetch_array($sql_result))
			{
				$selected_rows[]=$row;

			}	
			if(isset($selected_rows))
			{
				return $selected_rows;
			}
		}
		
//SQL ROWS CONVERT TO ARRAY ENDS

}

/**************************/	
//  DB functions Ends	  //
/*************************/	

/*********************************/	
// Pagination functions Starts   //
/*********************************/
{
//function-to-generate-a-pagination-link-list-in-php
// Function to generate pagination array - that is a list of links 
// for pages navigation 
	//Function pagination_data Start
	function pagination_data($base_url, $query_str, $total_pages, $current_page, $paginate_limit,$extra_qrys="")
	{ 
				 // Array to store page link list 
				 $page_array = array (); 
				 // Show dots flag - where to show dots? 
				 $dotshow = true; 
				 // walk through the list of pages 
				 for ( $i = 1; $i <= $total_pages; $i ++ ) 
				 { 
					 // If first or last page or the page number falls 
					 // within the pagination limit 
					 // generate the links for these pages 
					 if ($i == 1 || $i == $total_pages || ($i >= $current_page - $paginate_limit && $i <= $current_page + $paginate_limit) ) 
					 { 
						 // reset the show dots flag 
						 $dotshow = true; 
						 // If it's the current page, leave out the link 
						 // otherwise set a URL field also 
						 if ($i != $current_page)
						 {
							 if(empty($extra_qrys))
							 {
								 $page_array[$i]['url'] = $base_url.$query_str."/".$i;
							 }
							 else
							 {
								 $page_array[$i]['url'] = $base_url."?".$extra_qrys."&".$query_str."=".$i; 
							 }
						  $page_array[$i]['text'] = strval ($i); 
						 }
						 else
						 {
							 $page_array[$i]['url'] = "#";
						 	 $page_array[$i]['text'] = strval ($i); 
						 }
					 }
					  // If ellipses dots are to be displayed 
					  // (page navigation skipped) 
					  else if ($dotshow == true)
					  { 
						  // set it to false, so that more than one
						   // set of ellipses is not displayed 
						   $dotshow = false;
						   $page_array[$i]['text'] = "..."; 
					   } 
					} //For Loop ends
					if(count($page_array)>1)
					{
						 // return the navigation array 
				  		 return $page_array;
					}
					else
					{
						return true;	
					}
			   
	}
	//Function pagination_data End
	 
	//Function pagination_display Start			
	function pagination_display($pages)
	{ 	
	 	// list display 
		 foreach ($pages as $page) 
			{
					// If page has a link & Current page
					if((isset ($page['url']))&&($page['url']=='#')) 
					{
					?>
                         <li><a href="<?php echo $page['url']?>" class="current"> <?php echo $page['text'] ?> </a></li>
                    <?php
					}
					// If page has a link
					else if (isset ($page['url'])) 
					{
					?>
                         <li><a href="<?php echo $page['url']?>"> <?php echo $page['text'] ?> </a></li>
                    <?php
					}
					// no link - just display the text dot
					else
					{
					?>
                         <li><a href="#"><?php echo $page['text'] ?></a></li>
                    <?php
					}
			} //For each Pages Ends 
	 }
	//Function pagination_display Ends	
}
/*******************************/	
// Pagination functions Ends   //
/******************************/

/************************************/	
//  Misallonious functions Starts   //
/***********************************/	
{
//Email validation functions start //
	function check_email($email)
	{
		if(filter_var($mail, FILTER_VALIDATE_EMAIL))
		 {
		   return $email;
		 }
		else
		 {
		   $email=FALSE;
		   return $email;
		 }
	}
//Email validation functions Ends //

//clear symbols, CAPS in a string Function Starts
	function clear_data($string)
	{
			//Create a new name
			$chars = array("~", "!", "@", "#", "$", "%", "^", "&", "*", "(",")","-","+","=","{","}","[","]","|","\\",":",";","'","\"","<",">",",","/","?",".","_","0","1","2","3","4","5","6","7","8","9");
			$string=strtolower(str_replace($chars,"",$string));
			$string=trim($string);
			if(!empty($string))
			{
				return $string;
			}
			return false;
		}
//clear symbols, CAPS in a string Function Ends

//Create Random String with 10 Char -- START
	function random_string($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
//Random String function -- ENDS

//convert seo string -- START
	function convert_seo_string($str)
	{
		$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
        $extra_chars=array('and'=>'&','percentage'=>'%','at'=>'@','hash'=>'#','dollar'=>'$','star'=>'*'); 
        $search_char =  array_values($extra_chars);
        $replace_char= array_keys($extra_chars);
        $str = str_replace($search_char, $replace_char, $str);
	if($str !== mb_convert_encoding( mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') )
		$str = mb_convert_encoding($str, 'UTF-8', mb_detect_encoding($str));
	$str = htmlentities($str, ENT_NOQUOTES, 'UTF-8');
	$str = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\1', $str);
	$str = html_entity_decode($str, ENT_NOQUOTES, 'UTF-8');
	$str = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $str);
	$str = strtolower( trim($str, '-') );
	return $str;
	}
//convert seo string -- ENDS

//Get Current page URL -- STARTS
	function get_current_url()
	{
				$page_path=pathinfo($_SERVER['REQUEST_URI']);
				$page_name=$page_path['basename'];
		return	$desg_url=urlencode($page_name);	
	}
////Get Current page URL -- ENDS
}
/************************************/	
//  Misallonious functions Ends   //
/***********************************/	

?>