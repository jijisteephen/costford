<?php

// Block Direct Access
defined('PJT_EXE') or die('Access Restricted ,now the website is offline.');
	/**
	 * GET Database CONNECTION.
	 * since       08-Feb-2013 
	 * The minimum supported MySQL database version 5.0.4
	 */
		function select_my_db($database, $connection)
		{
			
				if (!$database)
				{
					return false;
				}
				else if (!mysqli_select_db($connection,$database))
				{
					return false;
				}
			return true;
		}		
		
		require_once("settings.php");	
		$config = new settings;
		$db_sfx=$config->dbprefix;
		$db_drive=$config->dbdrive;
		error_reporting(E_ALL);
		
		$servername = $config->host;
        $username = $config->user;
        $password = $config->password;
        $dbname = $config->database;
		if(!($config->offline))
		{
			echo($config->offline_message);
			exit();	
		}
		
			if(($config->dbdrive)=="mysqli")
				{
					if ( $conn=mysqli_connect($servername,$username, $password ))
					{	
						if((select_my_db($dbname, $conn)))
						{
							
							return true;	
						}
						else
						{
							echo(" Wrong Database");
							return false;
							exit();
						}
					}
					else
					{
						echo("Wrong Server Connection");
						exit();
					}
				}
			else if(($config->dbdrive)=="mysql")
				{
					if (  $conn=mysqli_connect($servername,$username, $password ))
					{	
						if(mysql_select_db($dbname, $conn))
						{	
							return true;	
						}
						else
						{
							echo(" Wrong Database");
							return false;
							exit();
						}
					}
					else
					{
						echo("Wrong Server Connection");
						exit();
					}
				} //DB CONNECT DRIVE CHECK
			
			
?>